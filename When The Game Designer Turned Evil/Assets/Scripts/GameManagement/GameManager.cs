﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    Scene curScene;

    Transform cam;
    GameObject player;
    PlayerStats playerStats;
    PlayerController playerController;
    EnemyProjectile enemyProjectileScript;

    public bool camFollowingPlayer;
    public bool camConstMove;

    public float camSpeed;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Application.persistentDataPath);

        curScene = SceneManager.GetActiveScene();

        if (curScene.buildIndex != 0)
        {
            cam = GameObject.Find("Main Camera").transform;

            player = GameObject.Find("Player");
            playerStats = player.GetComponent<PlayerStats>();
            playerController = player.GetComponent<PlayerController>();
            if (enemyProjectileScript != null)
            {
                enemyProjectileScript = GameObject.Find("Enemy").GetComponent<EnemyProjectile>();
            }

            if (curScene.buildIndex != 3)
            {
                CamFollowsPlayer();
            }
            else
            {
                ConstantMovingLevel();
            }

        }
        

        //Debug.Log("at player position");
    }

    // Update is called once per frame
    void Update()
    {


        if (camFollowingPlayer)
        {
            cam.Translate(player.transform.position - cam.position);
        }

        if (camConstMove)
        {
            cam.transform.position += Vector3.right * Time.deltaTime * camSpeed;
        }

        //Reset Scene
        if (Input.GetKeyDown("r"))
        {
            ReloadLevel();
        }
    }


    public void ConstantMovingLevel()
    {
        camFollowingPlayer = false;
        camConstMove = true;
    }
    public void CamFollowsPlayer()
    {
        camFollowingPlayer = true;
        camConstMove = false;
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    //save

    public void SavePlayer()
    {
        SaveSystem.SavePlayer(playerStats);
    }


    //load
    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        playerStats.curHealth = data.health;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

        player.transform.position = position;

    }



}
