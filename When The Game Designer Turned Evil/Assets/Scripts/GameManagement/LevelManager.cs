﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour
{
    Scene curScene;

    int curSceneIndex;

    GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        curSceneIndex = SceneManager.GetActiveScene().buildIndex;

        //Debug.Log(curSceneIndex);
        if (curSceneIndex == 3)
        {
            gm.ConstantMovingLevel();
        }
        else
        {
            gm.CamFollowsPlayer();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
