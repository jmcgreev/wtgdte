﻿using UnityEngine;
using System.IO; // Use System.IO when working with files from operation system
using System.Runtime.Serialization.Formatters.Binary;  //Allows acces of Binary formatters



public static class SaveSystem //Mark class as "static" so it can't be instantiated, and delete MonoBehavior so it isn't component of scene
{

    public static void SavePlayer(PlayerStats Stats)   //Use static to call it form anywhere without an instance
    {

        BinaryFormatter formatter = new BinaryFormatter(); //allows for binary conversion
        string path = Application.persistentDataPath + "/player.fun";  //a string path to store the save data to a new file. Creates a path to the data directory that wont change
        FileStream stream = new FileStream(path, FileMode.Create); //file stream of data containg a file

        PlayerData data = new PlayerData(Stats); // Create new set of data equal to current PlayerData


        formatter.Serialize(stream, data); //this inserts the data into the file

        stream.Close(); //need to close off data

    }


    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.fun";  //uses the path to find the data to load

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();   // allows for bianry conversion
            FileStream stream = new FileStream(path, FileMode.Open);  // a string path to open the saved data form the file

            PlayerData data = formatter.Deserialize(stream) as PlayerData; //changes from bianry back to readable format
            stream.Close();


            return data;
        }



        else  //if no file exists in the path
        {
            Debug.LogError("Save file not found in " + path);   //displays error message for the path
            return null;  //returns nothing
        }


    }



}
