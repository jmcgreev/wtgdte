﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {
    PlayerController PC;
    public bool isFacingRight;


	// Use this for initialization
	void Start ()
    {
        PC = GetComponent<PlayerController>();

        isFacingRight = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (PC.moveHorizontal > 0 && !isFacingRight)
        {
            FlipSprite();          
        }
        else if (PC.moveHorizontal < 0 && isFacingRight)
        {
            FlipSprite();
        }
        else
        {

        }

        
	}


    void FlipSprite()
    {
        isFacingRight = !isFacingRight;


        Vector3 theScale = transform.localScale;
        theScale.x *= -1;       
        transform.localScale = theScale;
    }
}
