﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingVolume : MonoBehaviour {

    public string targetSceneName;
    public bool useAsyncLoad = true;
    public bool stayLoaded = false;
    bool playerIsInVolume;

    private void Update()
    {
        if(playerIsInVolume)
        {
            ActiveSceneManager.LoadScene(targetSceneName, useAsyncLoad); 
        }
        else
        {
            if(!stayLoaded)
            {
                ActiveSceneManager.UnloadScene(targetSceneName);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerCharacter>() != null) //The player character script is empty it only acts like this because Rob wants to circumvent Unity's tag system
        {
            playerIsInVolume = true;
            Debug.Log("player entered");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerCharacter>() != null)
        {
            playerIsInVolume = false;
            Debug.Log("player left");
        }
    }
}
