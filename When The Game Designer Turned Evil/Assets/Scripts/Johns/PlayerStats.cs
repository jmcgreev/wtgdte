﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerStats : MonoBehaviour
{

    public int curHealth = 5;
    public int maxHealth = 5;

    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }

        if (curHealth <= 0)
        {
            Die();
        }

        //Debug.Log(curHealth);
    }

    void Die()
    {
        // Restart scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        // Death animation
        // Game over screen
    } 

    public void Damage(int dmg)
    {
        curHealth -= dmg;
    }

}
