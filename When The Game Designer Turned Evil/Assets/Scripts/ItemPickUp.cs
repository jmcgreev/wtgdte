﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    PlayerController playerController;

    public Item item;



    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            PickUp();
        }
    }

    void PickUp()
    {

        bool wasPickUp = Inventory.instance.Add(item);

        if (wasPickUp)
        {
            Destroy(gameObject);
            Debug.Log("Picking up " + item.name + ".");
        }
    }

}
