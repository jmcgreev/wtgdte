﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour
{
    public float sightDistance = 1.0f;//needs public value in editor //how far the debug.ray can see
    public float MaxSightDistance = 0.7f ; //how far the enemy can see


    Vector3 directionOfSight;
    Vector3 nmyCurPos;
    float sightOffset = 0.1f;
    public float RangeDistance = 1.5f;

    GameObject player;
    PlayerController playerControllerScript;
    EnemyController enemyControllerScript;
    EnemyProjectile enemyProjectileScript;

    public float ReloadTime = 3.0f;
    public float ShootTimer;

    LayerMask platformNplayer = (1 << 8| 1<< 9);

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerControllerScript = player.GetComponent<PlayerController>();
        enemyControllerScript = GetComponent<EnemyController>();
        enemyProjectileScript = GetComponent<EnemyProjectile>();
        
        ShootTimer = 0.0f; // set timer to 0
    }

    public void Update()
    {
        if (ShootTimer > 0.0f)
        {
            ShootTimer -= Time.deltaTime;
        }

        //Enemy current position
        nmyCurPos = transform.position;

        //ENEMY LINE OF SIGHT
        directionOfSight = player.transform.position - transform.position;

        /*
        if(directionOfSight.x / -directionOfSight.x != -1.0f)
        {
            directionOfSight = new Vector2(-directionOfSight.x, directionOfSight.y);
        }
        */

        Vector3 rayOrigin = new Vector3(nmyCurPos.x + sightOffset, nmyCurPos.y);

        //draws ray in editor
        Debug.DrawRay(rayOrigin, directionOfSight * sightDistance, Color.red, 0.1f, false);

        //raycast for tag detection
        RaycastHit2D sightTest = Physics2D.Raycast(rayOrigin, directionOfSight, sightDistance, platformNplayer);

        //distance to player
        float distanceX = (playerControllerScript.curPos.x - transform.position.x) * (playerControllerScript.curPos.x - transform.position.x);
        float distanceY = (playerControllerScript.curPos.y - transform.position.y) * (playerControllerScript.curPos.y - transform.position.y);
        float distanceToPlayer = Mathf.Sqrt(distanceX + distanceY);


        //enemy vision with raycast
        if (sightTest.collider)
        {
           //Debug.Log(sightTest.collider.tag);

            if(sightTest.collider.tag == "Platform")
            {
                Debug.Log("hit the:" + sightTest.collider.tag);
                enemyControllerScript.seesPlayer = false;
                enemyControllerScript.atStart = true;
            }

            else if (sightTest.collider.tag == "Player")
            {
                Debug.Log(distanceToPlayer);
                if (distanceToPlayer <= RangeDistance && ShootTimer <= 0.0f)
                {
                    enemyProjectileScript.Shoot();
                    ShootTimer = ReloadTime;
                }
                
                if (distanceToPlayer <= MaxSightDistance)
                {
                    enemyControllerScript.seesPlayer = true;
                }
                
                else if (distanceToPlayer > MaxSightDistance)
                {
                    enemyControllerScript.seesPlayer = false;
                }
                
            }
        }

    }
}
