﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int HP = 3;
    int curHP;

    public bool canBeHit;
    float TimeTillTakeDMG = 0.0f;
    float resetTime = 1.1f;
    EnemyController EC;

    // Start is called before the first frame update
    void Start()
    {
        EC = GetComponent<EnemyController>();
        curHP = HP;
        canBeHit = true;
    }

    // Update is called once per frame
    void Update()
    {

        if(HP != curHP) //Checks that enemy has taken damage
        {

            TimeTillTakeDMG = resetTime; //Sets the timer till can take damage again
            curHP = HP;
            canBeHit = false;
            EC.canMove = false;
        }

        if(TimeTillTakeDMG > 0) //if timer is a non-zero num then reduce timer
        {
            TimeTillTakeDMG -= Time.deltaTime;
        }
        if(TimeTillTakeDMG <= 0)  //resets timer if has dropped to zero or below
        {
            TimeTillTakeDMG = 0.0f;
            canBeHit = true;
            EC.canMove = true;
        }

    }
}
