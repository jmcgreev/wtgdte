﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float changeYVec;


    public float speed;
    public float runSpeed;
    public float jumpPower;


    public float jumpTime;
    public float jumpTimeCounter;

    public Rigidbody2D playerRB;

    bool isRunning;
    public bool isGrounded;

    public bool canMove;

    //WallJump Vars
    #region
    public float sideJumpPower;
    public float wallCheckRadius;
    public float wallJumpFallCounter = 0.0f;
    public float wallFallTime;

    public int wallJumpDir;

    public bool isJumping;
    public bool wantsToWallJumpRight;
    public bool wantsToWallJumpLeft;
    public bool isWallJumping;
    public bool hasAddedTime;

    public bool isFalling;

    public Transform leftWallJumpPos;
    public Transform rightWallJumpPos;
    #endregion


    public LayerMask whatIsGround;

    public Transform feetPos;
    public float groundCheckRadius;


    public Vector3 curPos;
    public Vector3 lastPos;

    public float moveHorizontal;

    float AtkHor;
    public float AtkVer;
    PlayerAttack PAtk;
    public bool canAtk;


    void Start()
    {
        playerRB = GetComponent<Rigidbody2D>();
        PAtk = GetComponentInChildren<PlayerAttack>();

        canMove = true;
        isRunning = false;
        isWallJumping = false;
        isFalling = false;

        canAtk = true;
    }


    void FixedUpdate()
    {


        curPos = transform.position;
        isGrounded = Physics2D.OverlapCircle(feetPos.position, groundCheckRadius, whatIsGround);

        wantsToWallJumpRight = Physics2D.OverlapCircle(leftWallJumpPos.position, wallCheckRadius, whatIsGround);
        wantsToWallJumpLeft = Physics2D.OverlapCircle(rightWallJumpPos.position, wallCheckRadius, whatIsGround);


        //Player Attack Direction
        AtkHor = Input.GetAxis("Horizontal");
        AtkVer = Input.GetAxis("Vertical");


        //Player Movement
        moveHorizontal = Input.GetAxis("Horizontal");


        if (canMove)
        {
            if (isRunning)
            {
                //Debug.Log("runing");
                playerRB.velocity = new Vector2((moveHorizontal * runSpeed), playerRB.velocity.y);
            }

            else
            {
                //Debug.Log("walking");
                playerRB.velocity = new Vector2((moveHorizontal * speed), playerRB.velocity.y);
            }
            //Running
            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
            {
                isRunning = true;
            }
            if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
            {
                isRunning = false;
            }
        }



        //WallJump
        if (!isGrounded)
        {
            if (wantsToWallJumpRight)
            {
                if (!hasAddedTime)
                {
                    AddTimeToFallCounter(wallFallTime);
                    hasAddedTime = true;
                }

                if (wallJumpFallCounter <= 0 && playerRB.velocity.y == 0.0f)
                {
                    playerRB.velocity = new Vector2(0, -Time.deltaTime);
                    isFalling = true;
                }


                if (Input.GetAxis("Horizontal") == 0)
                {
                    playerRB.velocity = new Vector2(0, playerRB.velocity.y * Time.deltaTime); //this make player feel lighter when on wall
                }



                jumpTimeCounter = 0.0f;
                wallJumpDir = 1;

                if (Input.GetButtonDown("Jump"))
                {
                    Debug.Log("Right wall jump");
                    isWallJumping = true;

                    //playerRB.velocity = new Vector2(playerRB.velocity.x, playerRB.velocity.y);
                }
            }

            else if (wantsToWallJumpLeft)
            {
                if (!hasAddedTime)
                {
                    AddTimeToFallCounter(wallFallTime);
                    hasAddedTime = true;
                }
                if (wallJumpFallCounter <= 0 && playerRB.velocity.y == 0.0f)
                {
                    playerRB.velocity = new Vector2(0, -Time.deltaTime);
                    isFalling = true;
                }


                if (Input.GetAxis("Horizontal") == 0)
                {
                    playerRB.velocity = new Vector2(0, playerRB.velocity.y * Time.deltaTime); //this make player feel lighter when on wall
                }
                jumpTimeCounter = 0.0f;
                wallJumpDir = -1;

                if (Input.GetButtonDown("Jump"))
                {
                    Debug.Log("Left wall jump");
                    isWallJumping = true;

                    //playerRB.velocity = new Vector2(playerRB.velocity.x, playerRB.velocity.y);
                }
            }

            if (isWallJumping)
            {
                wantsToWallJumpLeft = false;
                wantsToWallJumpRight = false;

                canMove = false;
                moveHorizontal = 0;

                playerRB.AddForce(new Vector3(wallJumpDir, changeYVec) * sideJumpPower);
                //playerRB.velocity = new Vector2((sideJumpPower * wallJumpDir), playerRB.velocity.y);
                isWallJumping = false;
                wallJumpFallCounter = 0;
                hasAddedTime = false;
            }

            else if (canAtk)
            {
                if (AtkVer < 0 && Input.GetButtonDown("Fire1"))
                {
                    canMove = false;
                    canAtk = false;
                    PAtk.SwordVerAttack(-1);
                }

            }
        }

        else if (isGrounded && wantsToWallJumpLeft || isGrounded && wantsToWallJumpRight)
        {
            wantsToWallJumpLeft = false;
            wantsToWallJumpRight = false;
        }

        // Regular Jump
        if (isGrounded)
        {
            canMove = true;
            isFalling = false;
            hasAddedTime = false;

            //PLAYER ATTACK INPUT
            if (canAtk)
            {
                //LeftAtk
                if (AtkHor < 0 && Input.GetButtonDown("Fire1"))
                {
                    canMove = false;
                    canAtk = false;
                    PAtk.SwordHorAttack(-1);
                }

                //RightAtk
                else if (AtkHor > 0 && Input.GetButtonDown("Fire1"))
                {
                    canMove = false;
                    canAtk = false;
                    PAtk.SwordHorAttack(1);
                }

                //UpAtk
                else if (AtkVer > 0 && Input.GetButtonDown("Fire1"))
                {
                    canMove = false;
                    canAtk = false;
                    PAtk.SwordVerAttack(1);
                }

            }


            playerRB.velocity = new Vector2(playerRB.velocity.x, playerRB.velocity.y);

            if (isGrounded == true && Input.GetButton("Jump"))
            {
                isJumping = true;
                jumpTimeCounter = jumpTime;
                playerRB.velocity = new Vector2(playerRB.velocity.x, jumpPower);
            }


        }

        if (isJumping == true && Input.GetButton("Jump"))
        {
            if (jumpTimeCounter > 0)
            {
                playerRB.velocity = new Vector2(playerRB.velocity.x, jumpPower);
                jumpTimeCounter -= Time.deltaTime;
            }

            else if (jumpTimeCounter < 0)
            {
                isJumping = false;
            }
        }


        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false;
        }

        lastPos = curPos;
    }


    public void Update()
    {
        if (wallJumpFallCounter > 0)
        {
            wallJumpFallCounter -= Time.deltaTime;
        }
        else if (wallJumpFallCounter < 0)
        {
            wallJumpFallCounter = 0;
        }
    }

    void AddTimeToFallCounter(float AddTime)
    {
        wallJumpFallCounter += AddTime;
    }



}
