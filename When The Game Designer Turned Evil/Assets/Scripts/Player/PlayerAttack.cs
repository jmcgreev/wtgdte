﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    BoxCollider2D swrdHurtBox;

    public Vector3 swrdHorRange;
    public Vector3 swrdVerRange;

    public Vector3 swrdHBSize;
    Vector3 initPos;
    Quaternion initRot;

    public int swrdDMG;

    PlayerController PC;

    public Vector3 BounceBackDir;

    public float BounceBackForce;

    void Start()
    {        
        PC = GetComponentInParent<PlayerController>();

        initPos = transform.localPosition;
        initRot = transform.localRotation;


    }

    public void SwordVerAttack(int dir)
    {
        BounceBackDir = new Vector3(0, dir);

        //UP ATTACK
        if (dir == 1)
        {
            transform.localPosition = swrdVerRange;
            //transform.Rotate(transform.rotation.x, transform.rotation.y, -45.0f);

            transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, 0.0f);

            swrdHurtBox = gameObject.AddComponent<BoxCollider2D>();
            swrdHurtBox.isTrigger = true;
            swrdHurtBox.offset = new Vector2(0.0f, 0.6f);
            swrdHurtBox.size = swrdHBSize;

            StartCoroutine(AttackCleanUp(1.0f));
        }


        //DOWN ATTACK
        else if (dir == -1)
        {
            transform.localPosition = -swrdVerRange;
            //transform.Rotate(transform.rotation.x, transform.rotation.y, -135.0f);
            
            transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, -180.0f);

            swrdHurtBox = gameObject.AddComponent<BoxCollider2D>();
            swrdHurtBox.isTrigger = true;
            swrdHurtBox.offset = new Vector2(0.0f, 0.6f);
            swrdHurtBox.size = swrdHBSize;

            StartCoroutine(AttackCleanUp(1.0f));
        }

    }

    public void SwordHorAttack(int dir)
    {
        BounceBackDir = new Vector3(dir, 0);



        // wanna take a vector to point the direction of hurt box
        // need oncollision trigger to deal dmg to enemy
        // clean up hurt box

        //LEFT ATTACK
        if (dir == -1)
        {
            transform.localPosition = -swrdHorRange;           
            //transform.Rotate(transform.rotation.x, 180.0f, -45.0f);

            transform.localEulerAngles = new Vector3(transform.rotation.x, 180.0f, -90.0f);

            swrdHurtBox = gameObject.AddComponent<BoxCollider2D>();
            swrdHurtBox.isTrigger = true;
            swrdHurtBox.offset = new Vector2(0.0f, 0.5f);
            swrdHurtBox.size = swrdHBSize;

            StartCoroutine(AttackCleanUp(1.0f));


        }



        //RIGHT ATTACK
        else if (dir == 1)
        {
            transform.localPosition = swrdHorRange;            
            //transform.Rotate(180.0f, transform.rotation.y, -45.0f);

            transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, -90.0f);

            swrdHurtBox = gameObject.AddComponent<BoxCollider2D>();
            swrdHurtBox.isTrigger = true;
            swrdHurtBox.offset = new Vector2(0.0f, 0.5f);
            swrdHurtBox.size = swrdHBSize;

            StartCoroutine(AttackCleanUp(1.0f));


        }

    }

    IEnumerator AttackCleanUp(float wait)
    {
        yield return new WaitForSeconds(wait);

        transform.localPosition = initPos;
        transform.localRotation = initRot;

        Destroy(GetComponent<BoxCollider2D>());
        PC.canMove = true;
        PC.canAtk = true;
    }


    void OnTriggerEnter2D(Collider2D coll)
    {


        if (coll.gameObject.tag == "Enemy")
        {
            if (coll.gameObject.GetComponent<EnemyHealth>().canBeHit)
            {
                coll.gameObject.GetComponent<EnemyHealth>().HP -= swrdDMG;

                PC.playerRB.AddForce(-BounceBackDir * BounceBackForce);
            }
        }
    }

}
