﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetPosController : MonoBehaviour
{
    PlayerController playerControllerScript; //Player coords
    public Vector2 desiredFeetPos; //feet coords

    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>(); //need to base off of player position
        
    }

    // Update is called once per frame
    void Update()
    {
        desiredFeetPos = new Vector2(playerControllerScript.curPos.x, (playerControllerScript.curPos.y - 0.64f)); // Continuous offset feet position from the Y axis
        transform.position = desiredFeetPos; //resets the current position to desired feetPos
    }
}
